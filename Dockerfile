FROM tomsimonr/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > cgdb.log'

COPY cgdb.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode cgdb.64 > cgdb'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' cgdb

RUN bash ./docker.sh
RUN rm --force --recursive cgdb _REPO_NAME__.64 docker.sh gcc gcc.64

CMD cgdb
